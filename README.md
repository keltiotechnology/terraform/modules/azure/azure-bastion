<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Azure Bastion

Terraform module which create a Bastion inside an existing public subnet

## Prerequisites

- Generate a pair of SSH key
You can use the following command line if you are on a Linux based OS
`ssh-keygen -t rsa -b 4096 -C "username@example.com" -f /your-target/path/id_rsa -N ''`
- Copy the content of the public key file from `/your-target/path/id_rsa.pub`
- Set the `ssh_public_key` attribute of the `bastion_vm` variable with the public key content
- Now you can access the Bastion VM by SSH using the private key `id_rsa`

## Module usage

```hcl
provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

module "azure_network" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks.git?ref=v1.1"

  ## Tags ##
  tags = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

module "azure_bastion" {
  source = "../.."
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Bastion VM ##
  bastion_subnet_id = module.azure_network.public_subnets["public_subnet_1"].id
  bastion_vm        = var.bastion_vm
  depends_on        = [module.azure_network]
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bastion_subnet_id"></a> [bastion\_subnet\_id](#input\_bastion\_subnet\_id) | ID of the subnet where the bastion will be located | `string` | n/a | yes |
| <a name="input_bastion_vm"></a> [bastion\_vm](#input\_bastion\_vm) | Bastion VM properties | `any` | <pre>{<br>  "disk": {<br>    "caching": "ReadWrite",<br>    "storage_account_type": "Standard_LRS"<br>  },<br>  "image": {<br>    "offer": "UbuntuServer",<br>    "publisher": "Canonical",<br>    "sku": "18_04-lts-gen2",<br>    "version": "latest"<br>  },<br>  "name": "bastionvm",<br>  "size": "Standard_B2s",<br>  "ssh_public_key": "your-ssh-public-key",<br>  "username": "bastion_admin"<br>}</pre> | no |
| <a name="input_location"></a> [location](#input\_location) | Azure region where the resource group will be created | `string` | `"france central"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Name of the resource group | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Resources Tags | `map(any)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_bastion_nic_id"></a> [bastion\_nic\_id](#output\_bastion\_nic\_id) | Outputs the bastion VM NIC ID |
| <a name="output_bastion_public_ip"></a> [bastion\_public\_ip](#output\_bastion\_public\_ip) | Outputs the bastion public IP for ssh access |
| <a name="output_bastion_public_ip_fqdn"></a> [bastion\_public\_ip\_fqdn](#output\_bastion\_public\_ip\_fqdn) | Outputs the bastion public IP fqdn |
| <a name="output_bastion_vm_admin_username"></a> [bastion\_vm\_admin\_username](#output\_bastion\_vm\_admin\_username) | Outputs the bastion VM admin username for ssh access |
| <a name="output_bastion_vm_id"></a> [bastion\_vm\_id](#output\_bastion\_vm\_id) | Outputs the bastion VM ID |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->