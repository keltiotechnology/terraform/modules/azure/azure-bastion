provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

module "azure_network" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks.git?ref=v1.1"

  ## Tags ##
  tags = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

module "azure_bastion" {
  source = "../.."
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Bastion VM ##
  bastion_subnet_id = module.azure_network.public_subnets["public_subnet_1"].id
  bastion_vm        = var.bastion_vm
  depends_on        = [module.azure_network]
}