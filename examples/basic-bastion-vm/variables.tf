## Providers ##
variable "subscription_id" {
  type        = string
  description = "Name of the resource group"
}

## Tags ##
variable "tags" {
  type        = map(any)
  description = "Resources Tags"
}

## Bastion VM
variable "bastion_vm" {
  type        = any
  description = "Bastion VM properties"
  default = {
    name           = "bastionvm"
    size           = "Standard_B2s"
    username       = "bastion_admin"
    ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCvDOrIe0OPMAdowpiL8he+SfJI11Nsw+h4ZUOoxmY1S0+xDuKTPsoZxOT4+176zBMcvFvv5JvayZaN6PexpuVXZWlSFvBraa5ivQP/yrtN0bzCxIj5jPP9Uy4aCOw7jXXIQOld8wL+Obsu8xijpZqfzHthP9+cotlQCmLlL+Z63psMYTYobAxaxt5CpL/AEbjxDM80fvmr9q8GnK6zw/Voj6xWm01eQq4JDWUgGXIkCO4ldCEiSQ9ZqdtP6/Cg0AguwrvycdGVDD0SHWeIjOx/waV2ethyyDo+E8FFJBjf8odSNz4ye5n10TlnCs34WFVVBnqzp+uf2XYkgF6WtDr3KNC7nPTtPH2Is9HqgDlTzDJgKmufCPezL1JO2gTFWWVQqd9yIdGPYRlheA+7dkAv1wqBtHn0Mx8k2rkO6oRTtOmjacsEDK9bfEW/JbgTtlQb+Qc4q7Q310CnjmEfZk/aidbB+Cs6hVOGerygZfe1pvx96DH9oHuzub9OBF0S+gtpufWVx7tMgbInFKQuvFa5/Im717Lq2zSd/KaglsMAEr7R4QN+O2L1Im1OrV4nPyzVL6/sQBcfZndzTk8E+26g/CF83GzEXB6yhWl6Kl1bhYGupRT8CqnJSdm7PLkmYbzt4quT3FeBCVFywYXXLMzN71WpJXLHmdzWHpi8keuxVQ== alphonse@azure-example.net"
    disk = {
      caching              = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }
    image = {
      publisher = "Canonical"
      offer     = "UbuntuServer"
      sku       = "18_04-lts-gen2"
      version   = "latest"
    }
  }
}

## Example target VM
variable "target_vm" {
  type        = any
  description = "Target VM properties"
  default = {
    name           = "targetvm"
    size           = "Standard_B2s"
    username       = "targadmin"
    ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCvDOrIe0OPMAdowpiL8he+SfJI11Nsw+h4ZUOoxmY1S0+xDuKTPsoZxOT4+176zBMcvFvv5JvayZaN6PexpuVXZWlSFvBraa5ivQP/yrtN0bzCxIj5jPP9Uy4aCOw7jXXIQOld8wL+Obsu8xijpZqfzHthP9+cotlQCmLlL+Z63psMYTYobAxaxt5CpL/AEbjxDM80fvmr9q8GnK6zw/Voj6xWm01eQq4JDWUgGXIkCO4ldCEiSQ9ZqdtP6/Cg0AguwrvycdGVDD0SHWeIjOx/waV2ethyyDo+E8FFJBjf8odSNz4ye5n10TlnCs34WFVVBnqzp+uf2XYkgF6WtDr3KNC7nPTtPH2Is9HqgDlTzDJgKmufCPezL1JO2gTFWWVQqd9yIdGPYRlheA+7dkAv1wqBtHn0Mx8k2rkO6oRTtOmjacsEDK9bfEW/JbgTtlQb+Qc4q7Q310CnjmEfZk/aidbB+Cs6hVOGerygZfe1pvx96DH9oHuzub9OBF0S+gtpufWVx7tMgbInFKQuvFa5/Im717Lq2zSd/KaglsMAEr7R4QN+O2L1Im1OrV4nPyzVL6/sQBcfZndzTk8E+26g/CF83GzEXB6yhWl6Kl1bhYGupRT8CqnJSdm7PLkmYbzt4quT3FeBCVFywYXXLMzN71WpJXLHmdzWHpi8keuxVQ== alphonse@azure-example.net"
    disk = {
      caching              = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }
    image = {
      publisher = "Canonical"
      offer     = "UbuntuServer"
      sku       = "18_04-lts-gen2"
      version   = "latest"
    }
  }
}