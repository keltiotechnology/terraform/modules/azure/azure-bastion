## Resource Group Variables ##
resource_group_name = "example_azure_resource_group"
location            = "West Europe"

## Virtual Network Variables ##
vnet_name    = "azure_net"
network_cidr = "10.0.0.0/16"

## Public Subnet Variables ##
public_subnets = {
  public_subnet_1 = {
    name                        = "public_subnet_1"
    address_prefixes            = ["10.0.1.0/24"]
    network_security_group_name = "public_subnet_nsg_1"
    disable_network_policies = {
      for_endpoint = false
      for_services = false
    }
    public_subnet_delegations    = {}
    public_subnet_security_rules = {}
  }
}

## Private Subnet Variables ##
private_subnets = {
  private_subnet_compute = {
    name             = "private_subnet_compute"
    address_prefixes = ["10.0.2.0/24"]
    disable_network_policies = {
      for_endpoint = false
      for_services = false
    }
    private_subnet_delegations = {}
  }
}

## NAT Gateway ##
public_subnet_gateway_ip = {
  name          = "public_subnet_ip"
  allocation    = "Static"
  sku           = "Standard"
  prefix_name   = "public_subnet_ip_prefix"
  prefix_length = 30
}

public_subnet_gateway = {
  name    = "public_subnet_gateway"
  sku     = "Standard"
  timeout = 10
}