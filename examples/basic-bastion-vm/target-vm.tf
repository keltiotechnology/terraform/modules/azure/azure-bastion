## A VM that you can access via SSH from the bastion VM
resource "azurerm_network_interface" "target_nic" {
  name                = "${var.target_vm.name}_nic"
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "${var.target_vm.name}_nic_cfg"
    subnet_id                     = module.azure_network.private_subnets["private_subnet_compute"].id
    private_ip_address_allocation = "Dynamic"
  }
  tags = var.tags
}

## Bastion VM ##
resource "azurerm_linux_virtual_machine" "target_vm" {
  name                  = var.target_vm.name
  location              = var.location
  resource_group_name   = var.resource_group_name
  size                  = var.target_vm.size
  admin_username        = var.target_vm.username
  network_interface_ids = [azurerm_network_interface.target_nic.id]

  admin_ssh_key {
    username   = var.target_vm.username
    public_key = var.target_vm.ssh_public_key
  }

  os_disk {
    caching              = var.target_vm.disk.caching
    storage_account_type = var.target_vm.disk.storage_account_type
  }

  source_image_reference {
    publisher = var.target_vm.image.publisher
    offer     = var.target_vm.image.offer
    sku       = var.target_vm.image.sku
    version   = var.target_vm.image.version
  }
  tags = var.tags
}