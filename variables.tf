## Tags ##
variable "tags" {
  type        = map(any)
  description = "Resources Tags"
}

## Resource Group Variables ##
variable "location" {
  type        = string
  description = "Azure region where the resource group will be created"
  default     = "france central"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group"
}

## Bastion VM
variable "bastion_subnet_id" {
  type        = string
  description = "ID of the subnet where the bastion will be located"
}

variable "bastion_vm" {
  type        = any
  description = "Bastion VM properties"
  default = {
    name           = "bastionvm"
    size           = "Standard_B2s"
    username       = "bastion_admin"
    ssh_public_key = "your-ssh-public-key"
    disk = {
      caching              = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }
    image = {
      publisher = "Canonical"
      offer     = "UbuntuServer"
      sku       = "18_04-lts-gen2"
      version   = "latest"
    }
  }
}