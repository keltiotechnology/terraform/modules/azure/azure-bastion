output "bastion_public_ip" {
  description = "Outputs the bastion public IP for ssh access"
  value       = azurerm_public_ip.bastion_public_ip.ip_address
}

output "bastion_public_ip_fqdn" {
  description = "Outputs the bastion public IP fqdn"
  value       = azurerm_public_ip.bastion_public_ip.fqdn
}

output "bastion_nic_id" {
  description = "Outputs the bastion VM NIC ID"
  value       = azurerm_network_interface.bastion_nic.id
}

output "bastion_vm_id" {
  description = "Outputs the bastion VM ID"
  value       = azurerm_linux_virtual_machine.bastion_vm.id
}

output "bastion_vm_admin_username" {
  description = "Outputs the bastion VM admin username for ssh access"
  value       = azurerm_linux_virtual_machine.bastion_vm.admin_username
}