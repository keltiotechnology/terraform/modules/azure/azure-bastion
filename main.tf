resource "azurerm_public_ip" "bastion_public_ip" {
  name                = "${var.bastion_vm.name}_public_ip"
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.tags
}

resource "azurerm_network_interface" "bastion_nic" {
  name                = "${var.bastion_vm.name}_nic"
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "${var.bastion_vm.name}_nic_cfg"
    subnet_id                     = var.bastion_subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion_public_ip.id
  }
  tags = var.tags
}

## Bastion VM ##
resource "azurerm_linux_virtual_machine" "bastion_vm" {
  name                  = var.bastion_vm.name
  location              = var.location
  resource_group_name   = var.resource_group_name
  size                  = var.bastion_vm.size
  admin_username        = var.bastion_vm.username
  network_interface_ids = [azurerm_network_interface.bastion_nic.id]

  admin_ssh_key {
    username   = var.bastion_vm.username
    public_key = var.bastion_vm.ssh_public_key
  }

  os_disk {
    caching              = var.bastion_vm.disk.caching
    storage_account_type = var.bastion_vm.disk.storage_account_type
  }

  source_image_reference {
    publisher = var.bastion_vm.image.publisher
    offer     = var.bastion_vm.image.offer
    sku       = var.bastion_vm.image.sku
    version   = var.bastion_vm.image.version
  }
  tags = var.tags
}

## Network security group for the Bastion VM NIC
resource "azurerm_network_security_group" "bastion_nsg" {
  name                = "${var.bastion_vm.name}_nsg"
  location            = var.location
  resource_group_name = var.resource_group_name

  security_rule {
    name                       = "allow-ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  tags = var.tags
}

resource "azurerm_network_interface_security_group_association" "bastion_nsg_to_nic" {
  network_interface_id      = azurerm_network_interface.bastion_nic.id
  network_security_group_id = azurerm_network_security_group.bastion_nsg.id
}